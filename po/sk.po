# Slovak translation for eog-plugins.
# Copyright (C) 2012 eog-plugins's COPYRIGHT HOLDER
# This file is distributed under the same license as the eog-plugins package.
# Richard Stanislavský <kenny.vv@gmail.com>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: eog-plugins master\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?"
"product=eog&keywords=I18N+L10N&component=plugins\n"
"POT-Creation-Date: 2017-08-10 20:28+0000\n"
"PO-Revision-Date: 2017-09-30 11:23+0200\n"
"Last-Translator: Dušan Kazik <prescott66@gmail.com>\n"
"Language-Team: Slovak <gnome-sk-list@gnome.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 1 : (n>=2 && n<=4) ? 2 : 0;\n"
"X-Generator: Poedit 2.0.3\n"

#: plugins/exif-display/eog-exif-display-plugin.c:105
msgid " (invalid Unicode)"
msgstr " (neplatný Unicode)"

#. TRANSLATORS: This is the actual focal length used when
#. the image was taken.
#: plugins/exif-display/eog-exif-display-plugin.c:183
#, c-format
msgid "%.1fmm (lens)"
msgstr "%.1fmm (objektív)"

#. Print as float to get a similar look as above.
#. TRANSLATORS: This is the equivalent focal length assuming
#. a 35mm film camera.
#: plugins/exif-display/eog-exif-display-plugin.c:194
#, c-format
msgid "%.1fmm (35mm film)"
msgstr "%.1fmm (35mm film)"

# GtkLabel label
#: plugins/exif-display/exif-display-config.ui:53
msgid "Histogram"
msgstr "Histogram"

# GtkCheckButton label
#: plugins/exif-display/exif-display-config.ui:88
msgid "Display per-channel histogram"
msgstr "Zobraziť histogram kanálu"

# GtkCheckButton label
#: plugins/exif-display/exif-display-config.ui:103
msgid "Display RGB histogram"
msgstr "Zobraziť histogram RGB"

# GtkLabel label
#: plugins/exif-display/exif-display-config.ui:135
msgid "General"
msgstr "Všeobecné"

# GtkCheckButton label
#: plugins/exif-display/exif-display-config.ui:170
msgid "Display camera settings in statusbar"
msgstr "Zobraziť nastavenia fotoaparátu v stavovom riadku"

# plugin name
#: plugins/exif-display/exif-display.plugin.desktop.in:5
#: plugins/exif-display/eog-exif-display.appdata.xml.in:6
msgid "Exif Display"
msgstr "Zobrazenie údajov Exif"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/exif-display/exif-display.plugin.desktop.in:8
msgid "zoom-fit-best"
msgstr "zoom-fit-best"

# plugin description
#: plugins/exif-display/exif-display.plugin.desktop.in:9
msgid "Displays camera settings and histogram"
msgstr "Zobrazuje nastavenia fotoaparátu a histogramu"

#: plugins/exif-display/eog-exif-display.appdata.xml.in:7
msgid "Displays Exif tags in the side panel and optionally the statusbar"
msgstr "Zobrazuje značky Exif v bočnom paneli a voliteľne v stavovej lište"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:59
msgid "ISO speed:"
msgstr "Citlivosť ISO:"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:74
msgid "Expo. Time:"
msgstr "Čas expozície:"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:89
msgid "Aperture:"
msgstr "Clona:"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:104
msgid "Zoom:"
msgstr "Priblíženie:"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:119
msgid "Metering:"
msgstr "Meranie:"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:134
msgid "Expo. bias:"
msgstr "Odchýlka expozície:"

# GtkLabel label
#: plugins/exif-display/exif-display.ui:149
msgid "Description:"
msgstr "Popis:"

# plugin name
#: plugins/export-to-folder/eog-export-to-folder.appdata.xml.in:6
#: plugins/export-to-folder/export-to-folder.plugin.desktop.in:6
msgid "Export to Folder"
msgstr "Export do priečinka"

# plugin description
#: plugins/export-to-folder/eog-export-to-folder.appdata.xml.in:7
#: plugins/export-to-folder/export-to-folder.plugin.desktop.in:10
msgid "Export the current image to a separate directory"
msgstr "Exportuje aktuálny obrázok do samostatného adresára"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/export-to-folder/export-to-folder.plugin.desktop.in:9
msgid "eog"
msgstr "eog"

# action
#: plugins/export-to-folder/export-to-folder.py:56
msgid "_Export"
msgstr "_Exportovať"

# gsettings summary
#: plugins/export-to-folder/org.gnome.eog.plugins.export-to-folder.gschema.xml.in:5
msgid "The folder to export to"
msgstr "Priečinok na exportovanie do"

# gsettings description
#: plugins/export-to-folder/org.gnome.eog.plugins.export-to-folder.gschema.xml.in:6
msgid ""
"This is the folder the plugin will place the exported files in. Plugin will "
"export to $HOME/exported-images if not set."
msgstr ""
"Toto je priečinok, do ktorého zásuvný modul umiestňuje exportované súbory. "
"Ak nie je nastavený, zásuvný modul ich bude umiestňovať do $HOME/exported-"
"images."

# GtkLabel label
#: plugins/export-to-folder/preferences_dialog.ui:19
msgid "Export directory:"
msgstr "Adresár pre export:"

# Plugin name
#: plugins/fit-to-width/eog-fit-to-width.appdata.xml.in:6
#: plugins/fit-to-width/fit-to-width.plugin.desktop.in:5
msgid "Zoom to Fit Image Width"
msgstr "Mierku zobrazenia prispôsobí šírke obrázku"

#: plugins/fit-to-width/eog-fit-to-width.appdata.xml.in:7
msgid "Adjusts the zoom to have the image’s width fit into the window"
msgstr "Upravuje priblíženie tak, aby šírka obrázku vyhovovala oknu"

# action entry
#: plugins/fit-to-width/eog-fit-to-width-plugin.c:129
msgid "Fit to width"
msgstr "Prispôsobiť šírke"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/fit-to-width/fit-to-width.plugin.desktop.in:8
msgid "gtk-zoom-fit"
msgstr "gtk-zoom-fit"

# plugin description
#: plugins/fit-to-width/fit-to-width.plugin.desktop.in:9
msgid "Fit images to the window width"
msgstr "Prispôsobuje obrázky šírke okna"

# plugin name
#: plugins/fullscreenbg/eog-fullscreenbg.appdata.xml.in:6
#: plugins/fullscreenbg/fullscreenbg.plugin.desktop.in:6
msgid "Fullscreen Background"
msgstr "Celoobrazovkové pozadie"

# plugin description
#: plugins/fullscreenbg/eog-fullscreenbg.appdata.xml.in:7
#: plugins/fullscreenbg/fullscreenbg.plugin.desktop.in:7
msgid "Enables changing background in fullscreen mode"
msgstr "Povolí zmenu pozadia v celoobrazovkovom režime"

# gsettings summary
#: plugins/fullscreenbg/org.gnome.eog.plugins.fullscreenbg.gschema.xml.in:5
msgid "Use custom background settings"
msgstr "Použiť vlastné nastavenia pozadia"

# gsettings description
#: plugins/fullscreenbg/org.gnome.eog.plugins.fullscreenbg.gschema.xml.in:6
msgid "Whether the plugin should use global settings or its own."
msgstr ""
"Či by mal zásuvný modul používať globálne nastavenia, alebo svoje vlastné."

# gsettings summary
#: plugins/fullscreenbg/org.gnome.eog.plugins.fullscreenbg.gschema.xml.in:10
msgid "Background color in fullscreen mode"
msgstr "Farba pozadia v celoobrazovkovom režime"

# gsettings description
#: plugins/fullscreenbg/org.gnome.eog.plugins.fullscreenbg.gschema.xml.in:11
msgid ""
"The color that is used to fill the area behind the image. This option has "
"effect only if use-custom is enabled."
msgstr ""
"Farba, ktorá sa použije na vyplnenie oblasti za obrázkom. Táto voľba má "
"vplyv iba ak je povolený kľúč use-custom."

# GtkCheckButton label
#: plugins/fullscreenbg/preferences_dialog.ui:17
msgid "Use custom color:"
msgstr "Použiť vlastnú farbu:"

# plugin name
#: plugins/hide-titlebar/eog-hide-titlebar.appdata.xml.in:6
#: plugins/hide-titlebar/hide-titlebar.plugin.desktop.in:5
msgid "Hide Titlebar"
msgstr "Skryť titulok okna"

# plugin description
#: plugins/hide-titlebar/eog-hide-titlebar.appdata.xml.in:7
msgid "Hides the titlebar of maximized windows"
msgstr "Skryje titulok okna pri maximalizovaných oknách"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/hide-titlebar/hide-titlebar.plugin.desktop.in:8
#: plugins/light-theme/light-theme.plugin.desktop.in:8
#: plugins/postasa/postasa.plugin.desktop.in:8
msgid "eog-plugin"
msgstr "eog-plugin"

# plugin description
#: plugins/hide-titlebar/hide-titlebar.plugin.desktop.in:9
msgid "Hides the titlebar of maximized Eye of GNOME windows"
msgstr "Skryje titulok okna pri maximalizovanom okne programu Oko GNOME"

# plugin name
#: plugins/light-theme/eog-light-theme.appdata.xml.in:6
#: plugins/light-theme/light-theme.plugin.desktop.in:5
msgid "Disable Dark Theme"
msgstr "Zakázať tmavú tému"

# plugin name
#: plugins/light-theme/eog-light-theme.appdata.xml.in:7
msgid "Disables dark theme"
msgstr "Zakáže tmavú tému"

# plugin description
#: plugins/light-theme/light-theme.plugin.desktop.in:9
msgid "Disables Eye of GNOME’s preference of dark theme variants"
msgstr "Zakáže nastavenie tmavých variant tém aplikácie Oko prostredia GNOME"

# sidebar page; plugin name
#: plugins/map/eog-map.appdata.xml.in:6 plugins/map/eog-map-plugin.c:467
#: plugins/map/map.plugin.desktop.in:5
msgid "Map"
msgstr "Mapa"

#: plugins/map/eog-map.appdata.xml.in:7
msgid "Displays on a map in the side panel where the picture was taken"
msgstr "Zobrazuje na mape v bočnom paneli, kde bol obrázok vytvorený"

# tooltip
#: plugins/map/eog-map-plugin.c:430
msgid "Jump to current image’s location"
msgstr "Prejde na umiestnenie aktuálneho obrázku"

# tooltip
#: plugins/map/eog-map-plugin.c:443
msgid "Zoom in"
msgstr "Zväčší"

# tooltip
#: plugins/map/eog-map-plugin.c:452
msgid "Zoom out"
msgstr "Zmenší"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/map/map.plugin.desktop.in:8
#| msgid "Map"
msgid "map"
msgstr "map"

# plugin description
#: plugins/map/map.plugin.desktop.in:9
msgid "Display the geolocation of the image on a map"
msgstr "Zobrazí geologické umiestnenie z obrázku na mape"

#: plugins/maximize-windows/eog-maximize-windows.appdata.xml.in:5
#: plugins/maximize-windows/maximize-windows.plugin.desktop.in:6
msgid "Maximize Windows"
msgstr "Maximalizácia okien"

#: plugins/maximize-windows/eog-maximize-windows.appdata.xml.in:6
msgid "New windows will open maximized"
msgstr "Nové okná budú otvárané v maximalizovanom stave"

#: plugins/maximize-windows/maximize-windows.plugin.desktop.in:7
msgid "Maximize new windows"
msgstr "Maximalizuje nové okná"

# plugin name
#: plugins/postasa/eog-postasa.appdata.xml.in:5
msgid "Picasa Web Uploader"
msgstr "Odovzdávanie na web služby Picasa"

#: plugins/postasa/eog-postasa.appdata.xml.in:6
msgid "Supports uploading photos to Google Picasa Web"
msgstr "Podpora odovzdávania fotografií do služby Google Picasa Web"

#: plugins/postasa/eog-postasa-plugin.c:306
msgid "Uploading…"
msgstr "Odovzdáva sa…"

#: plugins/postasa/eog-postasa-plugin.c:352
msgid "Uploaded"
msgstr "Odovzdané"

#: plugins/postasa/eog-postasa-plugin.c:356
msgid "Cancelled"
msgstr "Zrušené"

#: plugins/postasa/eog-postasa-plugin.c:359
msgid "Failed"
msgstr "Zlyhalo"

#: plugins/postasa/eog-postasa-plugin.c:582
#, c-format
msgid "Login failed. %s"
msgstr "Prihlásenie zlyhalo. %s"

#: plugins/postasa/eog-postasa-plugin.c:586
msgid "Logged in successfully."
msgstr "Úspešné prihlásenie."

#: plugins/postasa/eog-postasa-plugin.c:587
msgid "Close"
msgstr "Zavrieť"

#: plugins/postasa/eog-postasa-plugin.c:608
msgid "Cancel"
msgstr "Zrušiť"

#. TODO: want to handle passwords more securely
#: plugins/postasa/eog-postasa-plugin.c:614
msgid "Logging in…"
msgstr "Prihlasuje sa…"

#: plugins/postasa/eog-postasa-plugin.c:663
msgid "Please log in to continue upload."
msgstr "Ak chcete pokračovať v odovzdávaní, prosím, prihláste sa."

# action entry
#: plugins/postasa/eog-postasa-plugin.c:842
msgid "Upload to PicasaWeb"
msgstr "Odovzdať na PicasaWeb"

# GtkDialog title
#: plugins/postasa/postasa-config.ui:9
msgid "PicasaWeb Login"
msgstr "Prihlásenie na PicasaWeb"

# GtkButton label
#: plugins/postasa/postasa-config.ui:26
msgid "_Cancel"
msgstr "_Zrušiť"

# GtkButton label
#: plugins/postasa/postasa-config.ui:41
msgid "_Login"
msgstr "_Prihlásiť"

# GtkLabel label
#: plugins/postasa/postasa-config.ui:72
msgid "_Username:"
msgstr "Po_užívateľské meno:"

# GtkLabel label
#: plugins/postasa/postasa-config.ui:86
msgid "_Password:"
msgstr "_Heslo:"

# plugin name
#: plugins/postasa/postasa.plugin.desktop.in:5
msgid "PicasaWeb Uploader"
msgstr "Odovzdávanie na PicasaWeb"

# action entry tooltip; plugin description
#: plugins/postasa/postasa.plugin.desktop.in:9
msgid "Upload your pictures to PicasaWeb"
msgstr "Odovzdá vaše obrázky na PicasaWeb"

# GtkLabel label
#: plugins/postasa/postasa-uploads.ui:37
msgid "Uploads:"
msgstr "Odovzdávanie:"

# GtkTreeViewColumn title
#: plugins/postasa/postasa-uploads.ui:62
msgid "File"
msgstr "Súbor"

# GtkTreeViewColumn title
#: plugins/postasa/postasa-uploads.ui:84
msgid "Size"
msgstr "Veľkosť"

# GtkTreeViewColumn title
#: plugins/postasa/postasa-uploads.ui:96
msgid "Status"
msgstr "Stav"

# GtkButton label
#: plugins/postasa/postasa-uploads.ui:125
msgid "Cancel Selected"
msgstr "Zrušiť označené"

# GtkButton label
#: plugins/postasa/postasa-uploads.ui:138
msgid "Cancel All"
msgstr "Zrušiť všetko"

#: plugins/postr/eog-postr.appdata.xml.in:6
msgid "Postr"
msgstr "Postr"

#: plugins/postr/eog-postr.appdata.xml.in:7
msgid "Supports uploading photos to Flickr"
msgstr "Podpora odovzdávania fotografií na Flickr"

# action entry
#: plugins/postr/eog-postr-plugin.c:156
msgid "Upload to Flickr"
msgstr "Odovzdať na Flickr"

# plugin name
#: plugins/postr/postr.plugin.desktop.in:5
msgid "Flickr Uploader"
msgstr "Odovzdávanie na Flickr"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/postr/postr.plugin.desktop.in:8
#| msgid "Postr"
msgid "postr"
msgstr "postr"

# action entry tooltip; plugin description
#: plugins/postr/postr.plugin.desktop.in:9
msgid "Upload your pictures to Flickr"
msgstr "Odovzdá tvoje obrázky na Flickr"

# GtkLabel label
#: plugins/pythonconsole/config.ui:56
msgid "C_ommand color:"
msgstr "F_arba príkazu:"

# GtkLabel label
#: plugins/pythonconsole/config.ui:70
msgid "_Error color:"
msgstr "_Farba chyby:"

# plugin name
#: plugins/pythonconsole/eog-pythonconsole.appdata.xml.in:6
#: plugins/pythonconsole/__init__.py:87
#: plugins/pythonconsole/pythonconsole.plugin.desktop.in:6
msgid "Python Console"
msgstr "Konzola jazyka Python"

# plugin name
#: plugins/pythonconsole/eog-pythonconsole.appdata.xml.in:7
msgid "Adds a Python console"
msgstr "Pridá konzolu jazyka Python"

#: plugins/pythonconsole/__init__.py:59
msgid "P_ython Console"
msgstr "Konzola P_ython"

# gsetting summary
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:5
msgid "Command Text Color"
msgstr "Farba textu príkazu"

# gsetting description
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:6
msgid "The color used for commands."
msgstr "Farba použitá pre príkazy."

# gsetting summary
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:10
msgid "Error Text Color"
msgstr "Farba textu chyby"

# gsetting description
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:11
msgid "The color used for errors."
msgstr "Farba použitá pre chyby."

# gsetting summary
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:15
msgid "Whether to use the system font"
msgstr "Či sa má použiť systémové písmo"

# gsetting description
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:16
msgid ""
"If true, the terminal will use the desktop-global standard font if it’s "
"monospace (and the most similar font it can come up with otherwise)."
msgstr ""
"Ak je nastavené na true, terminál použije štandardné písmo pracovného "
"prostredia, ak je to neproporcionálne písmo (v opačnom prípade sa použije "
"najpodobnejšie písmo)."

# gsetting summary
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:24
msgid "Font used by Python Console"
msgstr "Písmo použité pre konzolu Python"

# gsetting description
#: plugins/pythonconsole/org.gnome.eog.plugins.pythonconsole.gschema.xml.in:25
msgid "A Pango font name. Examples are “Sans 12” or “Monospace Bold 14”."
msgstr "Názov písma pre Pango. Napríklad „Sans 12“ alebo „Monospace Bold 14“."

# plugin description
#: plugins/pythonconsole/pythonconsole.plugin.desktop.in:7
msgid "Python console for Eye of GNOME"
msgstr "Konzola jazyka Python pre program Oko GNOME"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/pythonconsole/pythonconsole.plugin.desktop.in:10
msgid "about"
msgstr "about"

# action entry
#: plugins/send-by-mail/eog-send-by-mail.appdata.xml.in:6
msgid "Send by Mail"
msgstr "Poslať emailom"

# plugin description
#: plugins/send-by-mail/eog-send-by-mail.appdata.xml.in:7
#: plugins/send-by-mail/send-by-mail.plugin.desktop.in:9
msgid "Sends an image attached to a new mail"
msgstr "Posiela obrázok ako prílohu nového emailu"

# action entry
#: plugins/send-by-mail/eog-send-by-mail-plugin.c:118
msgid "Send by _Mail"
msgstr "Odoslať _emailom"

# plugin name
#: plugins/send-by-mail/send-by-mail.plugin.desktop.in:5
msgid "Send By Mail"
msgstr "Poslať emailom"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/send-by-mail/send-by-mail.plugin.desktop.in:8
msgid "mail-send-symbolic"
msgstr "mail-send-symbolic"

# plugin name
#: plugins/slideshowshuffle/eog-slideshowshuffle.appdata.xml.in:6
#: plugins/slideshowshuffle/slideshowshuffle.plugin.desktop.in:6
msgid "Slideshow Shuffle"
msgstr "Premiešanie prezentácie"

# plugin description
#: plugins/slideshowshuffle/eog-slideshowshuffle.appdata.xml.in:7
msgid "Shuffles the photos in slideshow mode"
msgstr "Zamieša fotografie v režime prezentácie"

#. TRANSLATORS: Do NOT translate or transliterate this text!
#. This is an icon file name
#: plugins/slideshowshuffle/slideshowshuffle.plugin.desktop.in:9
msgid "media-playlist-shuffle"
msgstr "media-playlist-shuffle"

# plugin description
#: plugins/slideshowshuffle/slideshowshuffle.plugin.desktop.in:10
msgid "Shuffles images in slideshow mode"
msgstr "Zamieša snímky v režime prezentácie"

# tooltip
#~ msgid "Fit the image to the window width"
#~ msgstr "Prispôsobí obrázok šírke okna"

# action entry tooltip
#~ msgid "Send the selected images by mail"
#~ msgstr "Pošle vybrané obrázky emailom"
